from restaurant.models import Menu
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from restaurant.models import Menu
from restaurant.views import MenuItemView
from django.test import RequestFactory



class MenuViewTestCase(APITestCase):
    def setUp(self):
        self.factory = RequestFactory()
        # Generate a few items 
        self.menu1 = Menu.objects.create(title='test', price=10, inventory=10)
        self.menu2 = Menu.objects.create(title='test2', price=10, inventory=10)
        self.menu3 = Menu.objects.create(title='test3', price=10, inventory=10)
        self.menu4 = Menu.objects.create(title='test4', price=10, inventory=10)

    def test_menu_view(self):
        request = self.factory.get('/menu/')
        response = MenuItemView.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Menu.objects.count(), 4)
        

